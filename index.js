console.log('Hello!');

/*
	Objects
		- is a data type that is used to represent real world objects
		- it is also a collection of related data and/or functionalities

		Syntax:
			let objectName = {
				keyName: valueA,
				keyName: valueB
			}
*/

// Create an object
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999,
};

console.log(cellphone);
console.log(typeof(cellphone));

// Creating an object using constructor function
/*
	Constructors are like a blueprint pr templates to create our objects form

		Syntax:
			function ObjectName(keyA, keyB) {
				this.keyA = keyA;
				this.keyB = keyB
			};
*/

function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate
};

let laptop = new Laptop("Dell", 2008);
console.log(laptop);

let laptop2 = new Laptop("Acer", 2020);
console.log(laptop2);

let laptop3 = {
	name: "Asus",
	manufactureDate: 2020,
	price: 20000
};

let laptop4 = ["Asus", 2020, 20000];

console.log(laptop3);
console.log(laptop4);

function printName(name) {
	console.log(`My name is ${name}`);
};

printName("Dexter");

// Create empty object
let computer = {};
let myComputer = new Object();

console.log(computer);
console.log(myComputer);

// Accessing Object Properties
/*
		Syntax:
			objectName.propertyName;
*/
console.log(laptop.name);				// Dell
console.log(laptop.manufactureDate);	// 2008

// Using the square bracket
console.log(laptop["name"]);			// Dell

// Accessing Array Objects
let array = [laptop, laptop2];

console.log(array[0]['name']);
console.log(array[0].name);

// Initializing/adding/reassigning object properties
let car = {};
console.log(car);

car.name = 'Honda Civic';
console.log(car);

laptop.price = 30000;
console.log(laptop);

car['manufacture date'] = 2019;
console.log(car);

delete car['manufacture date'];
console.log(car);

laptop = {
	name: "Toshiba",
	manufactureDate: 2010
};
console.log(laptop);

// car = {};

car.name = "Ferrari";
console.log(car);

let user = {
	name: "Dexter",
	email: ["dex@mail.com", "dex1@mail.com"]
};
console.log(user.email[1]);

user.email = "Dixie";

console.log(user.email);				// Dixie

console.log(user.length);				// undefined

console.log(user.name.length);				// 6

// Object Methods
/*
	- a method is a function which is a property of an object
	- they are also function and one of the key differences they have is that they are functions related to a specific object
*/

let person = {
	name: "Casi",
	talk: function() {
		console.log("Hello my name is " + this.name);
	},
	/*walk: function() {
		console.log(this.name + " walked 25 steps.");
	}*/
};

console.log(person);
person.talk();

// Mini activity
/*
	Create a function named walk. "Casi walked 25 steps forward"
*/

person.walk = function() {
	console.log(this.name + " walked 25 steps.");
};

person.walk();

// Real world application of objects
/*
	Scenario:
		1. We would like to create a game that would have several pokemon interact with each other
		2. Every pokemon should have the same sets of stats, properties and functions
*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon;s health is now reduced to targetPokemonHealth");
	},
	faint: function() {
		console.log("Pokemon fainted");
	}
}

console.log(myPokemon);

function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
	};
	this.faint = function() {
		console.log(this.name + " fainted");
	}
};

let pikachu = new Pokemon("Pikachu", 16);
let charizard = new Pokemon("Charizard", 8);
console.log(pikachu);
console.log(charizard);

pikachu.tackle(charizard);